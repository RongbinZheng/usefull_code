f=$1
dat=`cat $f`
d1=10000
d2=1000
for f in $dat
do
	v1=`echo ${f#*dataset}`
	ID=`echo ${v1%/*}`
	echo $ID
	python RegPotential.py -t $f -d $d1 -n ./result_new/${ID}_${d1}.txt -g ./hg38.refGene
	python RegPotential.py -t $f -d $d2 -n ./result_new/${ID}_${d2}.txt -g ./hg38.refGene
done
