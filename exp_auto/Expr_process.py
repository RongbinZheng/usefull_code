import os,sys
import argparse
import urllib2
import re

def write_out(Command, server_type, ID = None, estimate = ['400', '1000']):
	"""write command line into file
	"""
	if not ID:
		ID = str(len([x for x in os.listdir('./') if x.endswith('.sbatch')])+1)
	if server_type == 'daisy':
		out = open('run_exp.sh', 'a')
		print >>out, Command
		out.close()
	if server_type == 'odyssey':
		sbatch_file = open('%s_exp.sbatch'%(ID), 'w')
		sbatch_file.write('#!/bin/bash \n\n')
		sbatch_file.write('#SBATCH -n 1  #Number of cores \n\n#SBATCH -N 1 \n\n#SBATCH -t %s  #Runtime in minutes \n\n'%estimate[0])
		sbatch_file.write('#SBATCH --mem=%s  #Memory per node in MB (see also --mem-per-cpu) \n\n#SBATCH -p serial_requeue \n\n#SBATCH -o %s_exp.log \n#SBATCH -J exp_%s \n\n'%(estimate[1], ID, ID))
		sbatch_file.write('source /n/home04/xiaoleliu/MEI/DC_env3/bin/activate\n')
		sbatch_file.write(Command)
		sbatch_file.close()
		out = open('run_sbatch.sh', 'a')
		print >>out, 'sbatch %s_exp.sbatch'%(ID)
		out.close()

def catfastq(main_path, gsm,srx_infor,srr,lay_type):
	"""get fastq download command 
	"""
	cmd = ''
	cat_file1 = ''
	cat_file2 = ''
	srx_infor = 'ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR'
	if lay_type == 'SINGLE':
		for i in range(len(srr)):
			srr[i] = srr[i][1:-12]
			ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] +'/' + srr[i]+ '.sra'
			# download the sra files and transmit them into fastq files
			fsra = '%s/%s_%s.sra'%(main_path, gsm, i+1)
			cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
			cmd = cmd + 'fastq-dump %s/%s_%s.sra -O %s/\n'%(main_path, gsm,i+1, main_path) 
			cat_file1 = cat_file1 + '%s/%s_%s.fastq '%(main_path, gsm,i+1) 
		cmd = cmd + 'cat %s> %s/%s.fastq \n'%(cat_file1,main_path,gsm) 
		cmd = cmd + 'rm %s/%s_* \n'%(main_path, gsm)
	elif lay_type == 'PAIRED':
		for i in range(len(srr)):
			srr[i] = srr[i][1:-12]
			ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] + '/' + srr[i]+ '.sra'
			fsra = '%s/%s_%s.sra'%(main_path, gsm, i+1)
			cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
			# cmd = cmd + 'python checkSRA.py %s\nif [ $? == 1 ];then\necho %s >> %s/record/SRAchecking_fail.xls\nexit\nfi\n'%(fsra, gsm+','+ftp+','+fsra, main_path)
			cmd = cmd + 'fastq-dump --split-files %s/%s_%s.sra -O %s\n'%(main_path, gsm,i+1, main_path) 
			cat_file1 = cat_file1 + '%s/%s_%s_1.fastq '%(main_path, gsm,i+1)
			cat_file2 = cat_file2 + '%s/%s_%s_2.fastq '%(main_path, gsm,i+1)
		cmd = cmd + 'cat %s> %s/%s.fastq_R1 \n'%(cat_file1, main_path,gsm) 
		cmd = cmd + 'cat %s> %s/%s.fastq_R2 \n'%(cat_file2, main_path,gsm) 
		cmd = cmd + 'rm %s/%s_* \n'%(main_path, gsm)
	return cmd
	
def _find_gse(doc_html):
	gse_regexp = re.compile('GSE\S*<')
	gse_infor = gse_regexp.search(doc_html)
	gse_infor = gse_infor.group().rstrip('</a>/')
	gse_url = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'%gse_infor
	gse_handler = urllib2.urlopen(gse_url)
	gse_html = gse_handler.read()
	srp_regexp = re.compile('ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP\S*<')
	srp_infor = srp_regexp.search(gse_html)
	srp_infor = srp_infor.group().rstrip('">(ftp)</a><')
	return srp_infor

def _parser_rnaseq(gsmid, output):
	"""parse rnaseq data, fastq link
	"""
	path, gsm = output, gsmid
	gsm_url = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'%gsm
	gsm_handler = urllib2.urlopen(gsm_url)
	gsm_html = gsm_handler.read()
#	srx_regexp = re.compile('ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX\S*"')
#	srx_infor = srx_regexp.search(gsm_html)
#	srx_infor = srx_infor.group().rstrip('"')
#	srx = srx_infor.split('/')[-1]
	# srx_regexp = re.compile('ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX\S*"')
	# srx_infor = srx_regexp.search(gsm_html)
	# srx_infor = srx_infor.group().rstrip('"')
	# srx = srx_infor.split('/')[-1]
	srx_regexp = re.compile('https://www.ncbi.nlm.nih.gov/sra\S*"')
	srx_infor = srx_regexp.search(gsm_html)
	if srx_infor:
		srx = srx_infor.group().rstrip('"').lstrip('https://www.ncbi.nlm.nih.gov/sra?term=')
	else:
		print "no srx of %s"%gsmid
		return None
	srx_url = 'http://www.ncbi.nlm.nih.gov/sra?term=%s'%srx
	srx_handler = urllib2.urlopen(srx_url)
	srx_html = srx_handler.read()
	lay_infor = re.compile('<div>Layout: <span>.{6}</span>')
	lay_type = lay_infor.search(srx_html)
	lay_type = lay_type.group()
	lay_type = lay_type[-13:-7]
	srr_regexp = re.compile('>SRR[0-9]*</a></td><td')
	srr = srr_regexp.findall(srx_html)
	#srp = _find_gse(gsm_html)
	if lay_type == 'SINGLE':
		fastq_file = '%s/%s.fastq'%(path,gsm)
	elif lay_type == 'PAIRED':
		fastq_file = '%s/%s.fastq_R1,%s/fastq_final/%s.fastq_R2'%(path,gsm,path,gsm)
	command = catfastq(path, gsm,srx_infor,srr,lay_type)
	return command+'\n'

def _parser_microarry(gsmid, output):
	"""parse CEL file link
	"""
	gsm_url = "https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s"%gsmid
	gsm_handler = urllib2.urlopen(gsm_url)
	gsm_html = gsm_handler.read()
	ftp_reg = re.compile('ftp://ftp.ncbi.nlm.nih.gov/geo/samples\S*"')
	ftp_infor = ftp_reg.search(gsm_html).group().rstrip('"')
	http_reg = re.compile('/geo/download/\S*"')
	cel_links = None
	if ftp_infor and 'CEL' in ftp_infor:
		cel_links = ftp_infor
	else:
		http_infor = http_reg.search(gsm_html).group().rstrip('"').replace('amp;', '')
		if http_infor and "CEL" in http_infor:
			cel_links = "https://www.ncbi.nlm.nih.gov" + http_infor
	if cel_links:
		if cel_links.endswith('gz'):
			cmd = 'wget %s -P %s\n'%(cel_links, output)
			cmd += 'gunzip '+ os.path.join(output, gsmid+'*'+'.gz')
			return cmd
		return 'wget %s -P %s'%(cel_links, output)
	return None
				

# def _parser(gsmList, data_type, server_type, outputDir):
# 	"""parse raw data, CEL file or SRA file, and get the download command
# 	"""
# 	if data_type == 'rna':
# 		## get the command of download
# 		cmd = ''
# 		for gsm in gsmList:
# 			cmd += _parser_rnaseq(gsm, output=outputDir)+"\n"
# 			write_out(Command=cmd, server_type=server, ID = gsm, estimate = ['400', '1000'])
# 	if data_type == 'array':
# 		cmd = ''
# 		for gsm in gsmList:
# 			link_microarray=_parser_microarry(gsm, output=outputDir)
# 			if link_microarray:
# 				cmd += link_microarray+'\n'
# 		write_out(Command=cmd, server_type=server, ID = gsm, estimate = ['400', '1000'])

# 	return None
def _run(server, data_type, inputFile, outputDir):
	"""get the input info, get command line, and assgin the task.
	"""
	if not os.path.exists(outputDir):
		os.mkdir(outputDir)
	with open(inputFile) as f:
		f = [x.rstrip().split('\t') for x in f]
		gsmList = [g[0] for g in f if g[0].startswith('GSM')]
		## parse raw data:
		if data_type == 'rna':
			## get the command of download
			cmd = ''
			for gsm in gsmList:
				cmd = _parser_rnaseq(gsm, output=outputDir)+"\n"
				write_out(Command=cmd, server_type=server, ID = gsm, estimate = ['400', '1000'])
		if data_type == 'array':
			cmd = ''
			for gsm in gsmList:
				link_microarray=_parser_microarry(gsm, output=outputDir)
				if link_microarray:
					cmd += link_microarray+'\n'
			write_out(Command=cmd, server_type=server, estimate = ['400', '1000'])


def main():
	try:
		parser = argparse.ArgumentParser(description="""automatically process expression microarry and RNA-seq""")
		#parser.add_argument( '-p', dest='path', type=str, required=False, help='the cistromeDC path, default is ./')
		parser.add_argument( '--server', dest='server', type=str, required=True,
		 help='select server type in [daisy, odyssey]')
		parser.add_argument( '-t', dest='type', type=str, required=True,
		 help='data type, array == microarry, rna == RNA-seq')
		parser.add_argument( '--matrix', dest='matrix', type=str, required=True,
		 help='the design matrix of the samples you want to process, with format: 1# column is GSMid, 2# column is condition which is grouped by number, eg: 0,1')
		parser.add_argument( '-o', dest='output', type=str, required=True,
		 help='the output path for saving the temp files and results')
		args = parser.parse_args()
		_run(server=args.server, data_type=args.type, inputFile=args.matrix, outputDir=args.output)
	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me T_T \n")
		sys.exit(0)

if __name__ == '__main__':
    main()
