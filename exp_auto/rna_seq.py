import os,sys
import argparse
import urllib2
import re

def write_out(Command, server_type, ID = None, estimate = ['400', '30000']):
	"""write command line into file
	"""
	if not ID:
		ID = str(len([x for x in os.listdir('./') if x.endswith('.sbatch')])+1)
	if server_type == 'daisy':
		out = open('run_exp.sh', 'a')
		print >>out, Command
		out.close()
	if server_type == 'odyssey':
		sbatch_file = open('%s_exp.sbatch'%(ID), 'w')
		sbatch_file.write('#!/bin/bash \n\n')
		sbatch_file.write('#SBATCH -n 8  #Number of cores \n\n#SBATCH -N 1 \n\n#SBATCH -t %s  #Runtime in minutes \n\n'%estimate[0])
		sbatch_file.write('#SBATCH --mem=%s  #Memory per node in MB (see also --mem-per-cpu) \n\n#SBATCH -p serial_requeue \n\n#SBATCH -o %s_exp.log \n#SBATCH -J exp_%s \n\n'%(estimate[1], ID, ID))
		sbatch_file.write('source /n/home04/xiaoleliu/MEI/DC_env3/bin/activate\n')
		sbatch_file.write(Command)
		sbatch_file.close()
		out = open('run_sbatch.sh', 'a')
		print >>out, 'sbatch %s_exp.sbatch'%(ID)
		out.close()

def _getcmd(ID, index, gtf, fastq, output):
	#mapping
	cmd1 = 'STAR --genomeDir {index} --runThreadN 8 --readFilesIn {fastq} --outFileNamePrefix {output} \n'.format(index=index,
	 fastq=fastq, output=os.path.join(output, ID))
	cmd2 = 'htseq-count -r pos -f sam -s no {sam} {gtf}  > {result} \n'.format(sam=os.path.join(output, ID+'Aligned.out.sam'),
		gtf=gtf, result=os.path.join(output, ID+'_count.txt'))
	return cmd1 + cmd2

def _run(server, fastq_dit, inputFile, outputDir, index, gtf):
	if not os.path.exists(outputDir):
		os.mkdir(outputDir)
	with open(inputFile) as f:
		f = [x.rstrip().split('\t') for x in f]
		gsmList = [g[0] for g in f if g[0].startswith('GSM')]
		for gsm in gsmList:
			fastqs = [os.path.join(fastq_dit, x) for x in os.listdir(fastq_dit) if x.startswith(gsm)]
			if not fastqs:
				print "no fastq files: %s"%gsm
				continue
			elif len(fastqs) == 1:
				fastq = fastqs[0]
			elif len(fastqs) == 2:
				fastq = ','.join(fastqs)
			else:
				print "more than two fastq files: %s"%gsm
				continue
			cmds = _getcmd(gsm, index, gtf, fastq, outputDir)
			write_out(Command=cmds, server_type=server, ID = gsm, estimate = ['400', '30000'])


def main():
	try:
		parser = argparse.ArgumentParser(description="""automatically process expression RNA-seq""")
		#parser.add_argument( '-p', dest='path', type=str, required=False, help='the cistromeDC path, default is ./')
		parser.add_argument( '--server', dest='server', type=str, required=True,
		 help='select server type in [daisy, odyssey]')
		parser.add_argument( '--matrix', dest='matrix', type=str, required=True,
		 help='the design matrix of the samples you want to process, with format: 1# column is GSMid, 2# column is condition which is grouped by number, eg: 0,1')
		parser.add_argument( '-fd', dest='fastq_dirtory', type=str, required=True,
		 help='the folder path of fastq')
		parser.add_argument( '-o', dest='output', type=str, required=True,
		 help='the output path for saving the temp files and results')
		parser.add_argument( '-g', dest='genome', type=str, required=True,
		 help='genome index fold of STAR')
		parser.add_argument( '-gtf', dest='gtf_file', type=str, required=True,
		 help='gtf file path for htseq-count')
		args = parser.parse_args()
		_run(server=args.server, fastq_dit=args.fastq_dirtory ,inputFile=args.matrix, outputDir=args.output, index=args.genome, gtf=args.gtf_file)
	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me T_T \n")
		sys.exit(0)

if __name__ == '__main__':
    main()
