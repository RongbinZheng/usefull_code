# This module prepared for preprocessing lisa files (peak location, regulatory potential, and read count hdf5 files for tf/histone/DNase/ATAC). #
# ----------------------------------------------- #
# RP_peakxls.py: use this function to get 5fold summit and regulatory potential of # transcription factor # by peak xls file of MACS output. #

```
python RP_peakxls.py -h
usage: RP_peakxls.py [-h] -f FILE -c COL -d DIRT [-s SP]

automatically get peak summit which is more than 5FC

optional arguments:
  -h, --help  show this help message and exit
  -f FILE     file path contains information of ID and peak file (peak.xls)
              path, seperate by tab.
  -c COL      the column number of ID and peak, start with 0, for example: 0,1
              means first column is ID and second is peak file path.
  -d DIRT     the directory path for saving 5fold change summits and RP
              result.
  -s SP       species of hg38 or mm10, or the column number in input file,
              start with 0.
```
# -----------------------------------------------#
# pre_summit.py: using this function to get peak locations of tf/histone/DNase/ATAC with 100bp resolution by norrow or broad peak bed files. #

```
python pre_summit.py -h
usage: pre_summit.py [-h] -f FILE -c COL -d DIRT -s SP

automatically get peak summit which is more than 5FC, and get 100bin index.

optional arguments:
  -h, --help  show this help message and exit
  -f FILE     file path contains information of ID and peak file (narrow or
              broad peak) path, seperate by tab.
  -c COL      the column number of ID and peak, start with 0, for example: 0,1
              means first column is ID and second is peak file path.
  -d DIRT     the directory path for saving 5fold change summits and peak location file.
  -s SP       species of hg38 or mm10.
```

# -----------------------------------------------#
# RP_bw.py: using this function to get regulatory potential of histone/DNase/ATAC by bigwigle files and LISA software.#

```
python RP_bw.py -h
usage: RP_bw.py [-h] -f FILE -c COL -d DIRT -s SP -t TYPE

automatically get RP from bigwig according factor type using lisa.

optional arguments:
  -h, --help  show this help message and exit
  -f FILE     file path contains information of ID and bigwig file (peak.xls)
              path, seperate by tab.
  -c COL      the column number of ID and bigwig, start with 0, for example:
              0,1 means first column is ID and second is bigwig file path.
  -d DIRT     the directory path for saving 5fold change summits and RP
              result.
  -s SP       species of hg38 or mm10, or the column number in input file,
              start with 0.
  -t TYPE     factor type of [H3K4me3, H3K27ac, H3K4me1, DNase, ATAC-seq]
```
 # -----------------------------------------------#
 # Finally, you need to combine the sample hd5 files into one, so that LISA can use it.#
 ## combine_new.py: using this function to merge hd5 files, tow sub-functions are included. ##
 
```
 python combine_new.py -h
usage: combine_new.py [-h] {run,check} ...

combine the pre-processed informtaion into hd5 file

positional arguments:
  {run,check}  sub-command help
    run        merge h5 files
    check      check files

optional arguments:
  -h, --help   show this help message and exit
```
 ## sub-function of "check": using this functions to check the requested files which generated from above three functions are really there.##

```
 python combine_new.py check -h
usage: combine_new.py check [-h] -f IFILE -c COL -d ADDITIONAL [-s SP] -t TYPE

check the preprocessed files, if not, process again

optional arguments:
  -h, --help     show this help message and exit
  -f IFILE       file path contains information of ID and needed file path,
                 seperate by tab.
  -c COL         the column number of ID, narrow / broad peak file, peakxls,
                 and bw file. start with 0, for example: 0,1,2,3 means first
                 column is ID and second is peak file path.
  -d ADDITIONAL  the folder of requested files, seperate by , if multiple
                 folder are there.
  -s SP          species of hg38 or mm10
  -t TYPE        factor type of [tf, H3K4me3, H3K27ac, H3K4me1, DNase, ATAC-
                 seq]
```
 ## sub-function of "run": merge files##
```
 python combine_new.py run -h
usage: combine_new.py run [-h] -f FILE -t TYPE -o SAVEFILE -d ADDITIONAL

process: merge RP or peak locations into h5 file.

optional arguments:
  -h, --help     show this help message and exit
  -f FILE        file path contains information of ID in the first column,
                 seperate by tab.
  -t TYPE        factor type, should one of in [tf, H3K4me3, H3K4me3, H3K27ac,
                 ATAC, DNase]
  -o SAVEFILE    the prefix of saving result
  -d ADDITIONAL  the folder of requested files, seperate by , if multiple
                 folder are there.
```

