import os,sys
import h5py
import pandas as pd
import numpy as np
import argparse
"""1. tf beta rp: 1006_beta_peakfold5.tab, 2. tf peak location: 1006_5fold_summit.index.bin.npy
/data5/home/rongbin/projects/LISA/human_factor_summits,/data5/home/rongbin/projects/LISA/mouse_factor_summits,/data5/home/rongbin/projects/LISA/tf_npy_summit
"""
# InputTable = sys.argv[1]
# savefile = sys.argv[2]

def _find_peak_location(ID, config):
	"""find 5fold summit location with 100bp resolution, npy file
	"""
	dir_list = config['dir_list'].split(',')
	for path in dir_list:
		peak_path = os.path.join(path, ID+'_5fold_summit.index.bin.npy')
		print(peak_path)
		if config['module'] == 'check':
			if os.path.exists(peak_path):
				return peak_path
		else:
			if os.path.exists(peak_path):
				peak_loc = np.load(peak_path)
				if peak_loc.shape != ():
					return peak_loc
	return None
	# peak_path = os.path.join('/data5/home/rongbin/projects/LISA/human_factor_summits', ID+'_5fold_summit.index.bin.npy')
	# if not os.path.exists(peak_path):
	# 	peak_path = os.path.join('/data5/home/rongbin/projects/LISA/mouse_factor_summits', ID+'_5fold_summit.index.bin.npy')
	# if not os.path.exists(peak_path):
	# 	peak_path = os.path.join('/data5/home/rongbin/projects/LISA/tf_npy_summit', ID+'_5fold_summit.index.bin.npy')
	# if not os.path.exists(peak_path):
	# 	print "no peak location result"
	# 	return None
	# peak_loc = np.load(peak_path)
	# if peak_loc.shape == ():
	# 	return None
	# return peak_loc

def _find_path(ID, config):
	"""match the RP file based on ID 
	"""
	dir_list = config['dir_list'].split(',')
	if config['Type'].lower() == 'tf':
		for path in dir_list:
			beta_rp_path = os.path.join(path, ID+'_beta_peakfold5.tab')
			if os.path.exists(beta_rp_path):
				os.system('echo %s'%beta_rp_path)
				return beta_rp_path
		os.system('echo "no beta RP result of %s"'%ID)
		return None
	if config['Type'].lower() in ['h3k4me3', 'h3k4me1', 'h3k27ac', 'dnase', 'atac']:
		#beta_rp_path, count_path = None, None
		for path in dir_list:
			beta_rp_path = os.path.join(path, '%s.%s.reg.h5' % (ID, config['Type']))
			if os.path.exists(beta_rp_path):
				break
			else:
				beta_rp_path = None
		for path in dir_list:
			count_path = os.path.join(path, '%s.%s.readcount.h5' % (ID, config['Type']))
			if os.path.exists(count_path):
				break
			else:
				count_path = None
		return beta_rp_path, count_path
	else:
		sys.stderr.write("out of knownledge: %s \n"%config['Type'])
		sys.exit(0)
	# beta_rp_path = os.path.join('/data5/home/rongbin/projects/LISA/human_factor_summits', ID+'_beta_peakfold5.tab')
	# if not os.path.exists(beta_rp_path):
	# 	beta_rp_path = os.path.join('/data5/home/rongbin/projects/LISA/mouse_factor_summits', ID+'_beta_peakfold5.tab')
	# if not os.path.exists(beta_rp_path):
	# 	print "no RP result"
	# 	return None
	# return beta_rp_path
def _get_sample_rp(beta_rp_path):
	"""get the RP based on path
	"""
	sample_values, refseq_index = [], []
	with open(beta_rp_path) as f:
		for line in f:
			if line.startswith('#'):
				continue
			line = line.rstrip().split('\t')
			refseq_index.append(':'.join(line[:4])+":"+line[-1])
			sample_values.append(float(line[4]))
	sample_rp = pd.DataFrame(sample_values,index=refseq_index)
	return sample_rp

def _process_tf(sampleList, config):
	"""merge peak location numpy file and beta rp file based on TF result.
	"""
	savefile = config['saveFile']
	all_rp, get_id = [], []
	#get the first sample and get the refseq order # go with peak_loc first
	#print str(sampleList[0])
	os.system('echo "start with summit location"')
	with h5py.File(savefile+'_peak_loct.h5',"a") as store_peak:
		for i in sampleList:
			i = str(i)
			peak_loct = _find_peak_location(i, config)
			if type(peak_loct) != type(np.array(1)):
				continue
			peak = store_peak.create_dataset(i, dtype=np.int32, shape=(len(peak_loct),), compression='gzip', shuffle=True, fletcher32=True)
			peak[...] = peak_loct
			store_peak.flush()
			get_id.append(i)
	with h5py.File(savefile+'_peak_loct.h5',"a") as store_peak:
		label = store_peak.create_dataset('IDs', dtype='S25', shape=(len(get_id),), compression='gzip', shuffle=True, fletcher32=True)
		label[...] = np.array(get_id)
		store_peak.flush()
	# go with beta rp
	os.system('echo "start with beta rp"')
	os.system('echo "check usefull id"')
	use_id = []
	for i in get_id:
		beta_rp_path = _find_path(i, config)
		if not beta_rp_path:
			continue
		use_id.append(i)
	os.system('echo "start to add new samples for %s"'%(len(use_id)))
	with h5py.File(savefile+'_beta_rp.h5',"a") as store:
		beta_rp_path = _find_path(use_id[0], config)
		sample_rp = _get_sample_rp(beta_rp_path)
		orders = sample_rp.index.tolist()
		refseq_arr = store.create_dataset("RefSeq", shape=(len(orders), ), dtype='S200', compression='gzip', shuffle=True, fletcher32=True)
		refseq_arr[...] = np.array(orders, dtype='S200')
		RP = store.create_dataset("RP", dtype=np.float32, shape=(len(orders), len(use_id)), compression='gzip', shuffle=True, fletcher32=True)
		ids = store.create_dataset("IDs", shape=(len(use_id), ), dtype='S50', compression='gzip', shuffle=True, fletcher32=True)
		iids = []
		for x, i in enumerate(use_id):
			os.system('echo %s'%i)
			beta_rp_path = _find_path(i, config)
			sample_rp = _get_sample_rp(beta_rp_path)
			sample_rp = sample_rp.loc[orders, : ]
			RP[:,x] = np.array(sample_rp[0].tolist(), dtype='float32').T
			store.flush()
			#iids.append(str.encode(i, 'utf-8'))
			iids.append(i)
		ids[...] = np.array(iids)
		store.flush()
def _process_hm_ca(sampleList, config):
	"""merge peak location numpy file and beta rp file based on histone or ca result.
	"""
	savefile = config['saveFile']
	get_id = []
	#print str(sampleList[0])
	os.system('echo "start with summit location"')
	with h5py.File(savefile+'_peak_loct.h5',"a") as store_peak:
		for i in sampleList:
			i = str(i)
			peak_loct = _find_peak_location(i, config)
			if type(peak_loct) != type(np.array(1)):
				continue
			peak = store_peak.create_dataset(i, dtype=np.int32, shape=(len(peak_loct),), compression='gzip', shuffle=True, fletcher32=True)
			peak[...] = peak_loct
			store_peak.flush()
			get_id.append(i)
	with h5py.File(savefile+'_peak_loct.h5',"a") as store_peak:
		label = store_peak.create_dataset('IDs', dtype='S25', shape=(len(get_id),), compression='gzip', shuffle=True, fletcher32=True)
		label[...] = np.array(get_id, dtype = 'S25')
		store_peak.flush()
	# find file path based on ID
	os.system('echo "+++find file path of IDs which beta rp files"')
	hdf5_reg, hdf5_readcount, use_id_reg, use_id_readcount = [], [], [], []
	for ID in get_id:
		x, y = _find_path(ID, config)
		if x:
			hdf5_reg.append(x)
			use_id_reg.append(ID)
		if y:
			hdf5_readcount.append(y)
			use_id_readcount.append(ID)
	#process reg potential
	os.system('echo "+++start process reg potential"')
	with h5py.File(hdf5_reg[0]) as inf:
		nrp = inf["RP"].shape[0]
		refseq = inf["RefSeq"][...]
	with h5py.File('%s.%s.reg.h5' % (config['saveFile'], config['Type']), "a") as store:
		refseq_arr = store.create_dataset("RefSeq", shape=(len(refseq), ), dtype='S200', compression='gzip', shuffle=True, fletcher32=True)
		refseq_arr[...] = refseq
		RP = store.create_dataset("RP", dtype=np.float32, shape=(nrp, len(hdf5_reg)), compression='gzip', shuffle=True, fletcher32=True)
		ids = store.create_dataset("IDs", shape=(len(hdf5_reg), ), dtype='S50', compression='gzip', shuffle=True, fletcher32=True)
		iids = []
		for i, d in enumerate(hdf5_reg):
			os.system('echo %s'%d)
			with h5py.File(d) as inf:
				RP[:,i] = inf["RP"][:,0]
				store.flush()
				iids.append(str(use_id_reg[i]))
		ids[...] = np.array(iids, dtype = 'S50')
		store.flush()
	#process readcount
	os.system('echo "+++start process read count files"')
	with h5py.File(hdf5_readcount[0]) as inf:
		nc = inf["OrderCount"].shape[0]
	with h5py.File('%s.%s.readcount.h5' % (config['saveFile'], config['Type']), "a") as store:
		ct = store.create_dataset("OrderCount", dtype=np.float32, shape=(nc, len(hdf5_readcount)), compression='gzip', shuffle=True, fletcher32=True)
		ids = store.create_dataset("IDs", shape=(len(hdf5_readcount), ), dtype='S50', compression='gzip', shuffle=True, fletcher32=True)
		iids = []
		for i, d in enumerate(hdf5_readcount):
			with h5py.File(d) as inf:
				ct[:,i] = inf["OrderCount"][:,0]
				store.flush()
				iids.append(str(use_id_readcount[i]))
		ids[...] = np.array(iids)
		store.flush()

def _run(config):
	"""handle input information and process by type.
	"""
	InputFile = config['InputFile']
	if os.path.exists(InputFile):
		dat = pd.read_table(InputFile, header = None, sep = '\t') # meta data
	else:
		sys.stderr.write("The path do not exist: %s \n"%InputFile)
		sys.exit(0)
	sampleList=dat[0].tolist()
	if config['Type'].lower() == 'tf':
		_process_tf(sampleList, config)
	else:
		_process_hm_ca(sampleList, config)

def main():
	try:
		parser = argparse.ArgumentParser(description="""combine the pre-processed informtaion into hd5 file""")
		sub_parsers = parser.add_subparsers(help="sub-command help", dest="sub_command")
		parser_run = sub_parsers.add_parser("run", help="merge h5 files", description="process: merge RP or peak locations into h5 file.")
		parser_run.add_argument( '-f', dest='file', type=str, required=True,
		 help='file path contains information of ID in the first column, seperate by tab.')
		parser_run.add_argument( '-t', dest='type', type=str, required=True,
		 help='factor type, should one of in [tf, H3K4me3, H3K4me3, H3K27ac, ATAC, DNase]')
		parser_run.add_argument( '-o', dest='saveFile', type=str, required=True,
		 help='the prefix of saving result')
		parser_run.add_argument( '-d', dest='additional', type=str, required=True,
		 help='the folder of requested files, seperate by , if multiple folder are there.')

		parser_check = sub_parsers.add_parser("check", help="check files", description="check the preprocessed files, if not, process again")
		parser_check.add_argument( '-f', dest='ifile', type=str, required=True,
		 help='file path contains information of ID and needed file path, seperate by tab.')
		parser_check.add_argument( '-c', dest='col', type=str, required=True,
		 help='the column number of ID, narrow / broad peak file, peakxls, and bw file. start with 0, for example: 0,1,2,3 means first column is ID and second is peak file path.')
		parser_check.add_argument( '-d', dest='additional', type=str, required=True,
		 help='the folder of requested files, seperate by , if multiple folder are there.')
		parser_check.add_argument( '-s', dest='sp', type=str, required=False,
		 help='species of hg38 or mm10')
		parser_check.add_argument( '-t', dest='type', type=str, required=True,
		 help='factor type of [tf, H3K4me3, H3K27ac, H3K4me1, DNase, ATAC-seq]')

		args = parser.parse_args()
		#args = parser_run.parse_args()
		if args.sub_command == 'run':
			config = {'module':'run','InputFile':args.file, "Type":args.type, "saveFile":args.saveFile, "dir_list":args.additional}
			_run(config)
		if args.sub_command == "check":
			config = {'module':'check','InputFile':args.ifile, 'cols':args.col, 'dir_list':args.additional, 'species':args.sp, 'Type':args.type}
			dat = pd.read_table(config['InputFile'], header = None, sep = '\t')
			IDcol, peakCol, peakxlsCol, bwCol = [int(x) for x in config['cols'].split(',')]
			if config['Type'] == 'tf':
				for ID in dat[IDcol].tolist():
					ID = str(ID)
					#peak location
					p = _find_peak_location(ID, config)
					if not p:
						out = open('./try_again/try_again.tmp', 'w')
						out.write(out, ID+'\t'+dat.loc[dat[IDcol]==int(ID),peakCol].tolist()[0])
						out.close()
						cmd = 'python /data5/home/rongbin/projects/LISA/code/pre_summit.py -f {file} -c {cols} -d {dirt} -s {sp}'.format(file='./try_again/try_again.tmp',
							cols = '0,1', dirt = './try_again', sp = config['species'])						
						os.system('echo "try again of peak location of %s"'%ID)	
						os.system(cmd)
						# RP of tf
					else:
						os.system('echo "ok peak location of %s"'%ID)	
					p1 = _find_path(ID, config)
					if not p1:
						out = open('./try_again/try_again.tmp', 'w')
						out.write(ID+'\t'+dat.loc[dat[IDcol]==int(ID),peakxlsCol].tolist()[0]+'\n')
						out.close()
						cmd = 'python /data5/home/rongbin/projects/LISA/code/RP_peakxls.py -f {file} -c {cols} -d {dirt} -s {sp}'.format(file='./try_again/try_again.tmp',
							cols = '0,1', dirt = './try_again', sp = config['species'])
						os.system('echo "try again of RP calculation of %s"'%ID)	
						os.system(cmd)
					else:
						os.system('echo "ok RP of %s"'%ID)	
			if config['Type'] != 'tf':	
				for ID in dat[IDcol].tolist():
					ID = str(ID)
					#peak location
					p = _find_peak_location(ID, config)
					if not p:
						out = open('./try_again/try_again.tmp', 'w')
						out.write(ID+'\t'+dat.loc[dat[IDcol]==int(ID),peakCol].tolist()[0]+'\n')
						out.close()
						cmd = 'python /data5/home/rongbin/projects/LISA/code/pre_summit.py -f {file} -c {cols} -d {dirt} -s {sp}'.format(file='./try_again/try_again.tmp',
							cols = '0,1', dirt = './try_again', sp = config['species'])
						os.system('echo "try again of peak location of %s"'%ID)						
						os.system(cmd)
					else:
						os.system('echo "ok peak location of %s"'%ID)
					p1, p2 = _find_path(ID, config)
					if p1 and p2:
						os.system('echo "ok bw RP of %s"'%ID)
					else:
						out = open('./try_again/try_again.tmp', 'w')
						out.write(ID+'\t'+dat.loc[dat[IDcol]==int(ID),bwCol].tolist()[0]+'\n')
						out.close()
						cmd = 'python /data5/home/rongbin/projects/LISA/code/RP_bw.py -f {file} -c {cols} -d {dirt} -s {sp} -t {type}'.format(file='./try_again/try_again.tmp',
							cols = '0,1', dirt = './try_again', sp = config['species'], type = config['Type'])
						os.system('echo "try again of RP calculation of %s from bw"'%ID)	
						os.system('source /data/home/qqin/miniconda3/envs/lisa2/bin/activate lisa2\n'+cmd)
						

	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me T_T \n")
		sys.exit(0)

if __name__ == '__main__':
    main()



#python combine_new.py -f table/dnase_mm.xls.select -t DNase -o ./dnase_mm -d /data5/home/rongbin/projects/LISA/bw_rp,/data5/home/rongbin/projects/LISA/hg_bw_rp,/data5/home/rongbin/projects/LISA/hisca_result,/data5/home/rongbin/projects/LISA/human_factor_summits,/data5/home/rongbin/projects/LISA/mouse_factor_summits,/data5/home/rongbin/projects/LISA/tf_npy_summit

#python combine_new.py check -f ./table/dnase_mm.xls.select -c 0,17,18,19 -d /data5/home/rongbin/projects/LISA/bw_rp,/data5/home/rongbin/projects/LISA/hg_bw_rp,/data5/home/rongbin/projects/LISA/hisca_result,/data5/home/rongbin/projects/LISA/human_factor_summits,/data5/home/rongbin/projects/LISA/mouse_factor_summits,/data5/home/rongbin/projects/LISA/tf_npy_summit -s mm10 -t D

#./table/dnase_mm.xls.select -c 0,17,18,19 -d /data5/home/rongbin/projects/LISA/bw_rp,/data5/home/rongbin/projects/LISA/hg_bw_rp,/data5/home/rongbin/projects/LISA/hisca_result,/data5/home/rongbin/projects/LISA/human_factor_summits,/data5/home/rongbin/projects/LISA/mouse_factor_summits,/data5/home/rongbin/projects/LISA/tf_npy_summit -s mm10 -t DNase



