import os,sys
import argparse
import pandas as pd
import numpy as np
import commands
#import cPickle as pk

def _run(inputfile, columns, outputDir, sp='hg38'):
	"""read information from file, for each summit file to get 5fold summits and save into new file.
	"""
	windows="/data/home/qqin/seqpos2/mm10_window100bp.bed"
	columns = columns.split(',')
	ID_col, sumit_col = int(columns[0]), int(columns[1])
	with open(inputfile) as f:
		for line in f:
			line = line.rstrip().split('\t')
			ID, path = line[ID_col], line[sumit_col]
			os.system('echo %s'%ID)
			#print ID
			# get the 5fold peak
			final_file = os.path.join(outputDir, ID+"_5fold_summit.bed")
			if not os.path.exists(final_file):
				tmp_file = final_file+'.tmp'
				cmd1 = "awk '{if($7>5){print $0}}' %s > %s"%(path, tmp_file) 
				cmd2 = "awk -vOFS='\t' '{$2=$2+(int(($3-$2)/2))-1; $3=$2+1} {print $0}' %s |cut -f1,2,3,4,9 > %s"%(tmp_file, final_file)
				cmd3 = "rm "+tmp_file
				#cmd = cmd1+'\n'+cmd2+'\n'+cmd3+'\n'
				os.system(cmd1+'\n'+cmd2+'\n'+cmd3+'\n')
				#get the new peak number
				peak_number = commands.getoutput("wc -l %s"%final_file).split(' ')
				#line.append(peak_number)
			#get 100bp bin index
			if sp == 'mm10':
				window_file = '/data/home/qqin/seqpos2/mm10_window100bp.bed'
			else:
				window_file = '/data/home/qqin/seqpos2/hg38_window100bp.bed'
			bin_file = os.path.join(outputDir, ID+'_5fold_summit.index.bin')
			cmd = "bedtools intersect -wa -u -a %s -b %s | cut -f 4 > %s" % (window_file,final_file,bin_file)
			r = commands.getoutput(cmd)
			if r.startswith('Unexpected file format') or r.startswith('Error'):
				bin_file = 'NA'
				npfile = 'NA'
			else:
				a=np.loadtxt('%s' % bin_file, dtype='int32')
				np.save('%s' % bin_file, a)
				npfile = bin_file+'.npy'
			out1 = open(inputfile+'.inf', 'a')
			print >>out1, '\t'.join(line+[bin_file, npfile])
			out1.close()
			#run lisa RP
			# RP_res_path = os.path.join(outputDir, ID + "_beta_peakfold5.tab") 
			# if sp == 'mm10':
			# 	tss_file = '/data/home/qqin/MARGE/scripts/marge2/histonerp/mm10.tss'
			# else:
			# 	tss_file = '/data/home/qqin/MARGE/scripts/marge2/histonerp/hg38.tss'
			# cmd = "python /data5/home/chenfei/JingyuFan/data_collection/MARGE/LISA_figures/RegPotential_lisa_weight.py -n %s -g %s -t %s " % (RP_res_path, tss_file, final_file)
			# out2 = open(inputfile+'.sh', 'a')
			# print >>out2, cmd
			# out2.close()

def main():
	try:
		parser_run = argparse.ArgumentParser(description="""automatically get peak summit which is more than 5FC, and get 100bin index.""")
		parser_run.add_argument( '-f', dest='file', type=str, required=True,
		 help='file path contains information of ID and peak file (narrow or broad peak) path, seperate by tab.')
		parser_run.add_argument( '-c', dest='col', type=str, required=True,
		 help='the column number of ID and peak, start with 0, for example: 0,1 means first column is ID and second is peak file path.')
		parser_run.add_argument( '-d', dest='dirt', type=str, required=True,
		 help='the directory path for saving 5fold change summits and peak location file.')
		parser_run.add_argument( '-s', dest='sp', type=str, required=True,
		 help='species of hg38 or mm10.')

		args = parser_run.parse_args()

		_run(inputfile=args.file, columns=args.col, outputDir=args.dirt, sp = args.sp)
	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me T_T \n")
		sys.exit(0)

if __name__ == '__main__':
    main()
